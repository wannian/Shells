#-*-coding:utf-8-*-
#!/usr/bin/python

'''
建立图片站,处理爬取图片的水印
截取原有图片的水印
'''

from PIL import Image
import os

From_dir = 'G:/From'
To_dir = 'G:/To'

def cropImageBySize(full_name, save_name):
	img = Image.open(full_name)
	width, height = img.size
	img_format = img.format
	# 裁剪区域，裁掉右下方23px
	region = (0, 0, width, height-23)
	cropImg = img.crop(region)
	cropImg.save(save_name, img_format, quality=90)

def cropImageByRate(full_name, save_name):
	img = Image.open(full_name)
	width, height = img.size
	img_format = img.format
	# 裁减掉 %4的区域
	region = (0, 0, width, int(height*0.96))
	cropImg = img.crop(region)
	cropImg.save(save_name, img_format, quality=90)


if __name__ == "__main__":
	for file_name in os.listdir(From_dir):
		full_name = os.path.join(From_dir, file_name)
		if not os.path.isfile(full_name):
			continue
		# console 显示
		print(full_name + '\r\n')
		cropImageByRate(full_name, os.path.join(To_dir, file_name))

		
